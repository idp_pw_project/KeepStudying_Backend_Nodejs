// const Tutorial = db.tutorials;
const Activity= require('../models/activitiesModel');
const User= require('../models/userModel');

// Create and Save a new activity
exports.create = (req, res) => {
	//  Validate request
    if (!req.body.title) {
        res.status(400).send({message: "Content can not be empty!"});
        return;
    }
    const query = req.body.title;
    console.log("create");
    var teacherName = "";
    console.log(req.body.userId);
    User.find({"userId": req.body.userId}).then(data => {
        if (!data)
            res.status(404).send({
                message: "Not found user with userId " + userId
            });
        else {
            console.log(data);
            const {name} = data[0];
            console.log(name);
            teacherName = name;
            console.log(teacherName);
        };
    })
    .catch(err => {
        res.status(500)
            .send({
                message: "Error retrieving user with userId=" + userId
            });
    }).then(
        Activity.findOne({title:query}, function(err, activity) {
            if(err) console.log(err);
            if (activity){
                console.log("This has already been saved");
                res.status(400).send({message: "This has already been saved"});
                return;
            } else {
                // Create an Activity
                const activity = new Activity({
                    title: req.body.title,
                    userId: req.body.userId,
                    subject: req.body.subject,
                    grade: req.body.grade,
                    date: req.body.date,
                    time: req.body.time,
                    link: req.body.link,
                    maxNoStud: req.body.maxNoStud,
                    teacher: teacherName,
                });


                // Save Activiy in the database
                activity
                    .save(activity)
                    .then(data => {
                        console.log(data);
                        res.send(data);
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send({
                            message:
                                err.message
                                || "Some error occurred while creating the activity."
                        });
                    });
            }
        })
    )
   
};

// Retrieve all activities from the database.
exports.findAll = async (req, res) => {
	await Activity.find()
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message
					|| "Some error occurred while retrieving activities."
			});
		});
};

exports.findAllByTeacher = async (req, res) => {
    const {userId} = req.params;

    await Activity.find({"userId" : userId})
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message
					|| "Some error occurred while retrieving activities."
			});
		});
};

exports.findAllByGrade = async (req, res) => {
    const {grade} = req.params;

    await Activity.find({"grade" : grade})
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message
					|| "Some error occurred while retrieving activities."
			});
		});
};

// Find a single activity with an id
exports.findOne = async (req, res) => {
    const {title} = req.params;
    // console.log(condition);
    await Activity.findOne({"title" : title})
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Not found activity with title " + title
                });
            else res.send(data);
        })
        .catch(err => {
            res.status(500)
                .send({
                    message: "Error retrieving activity with title=" + title
                });
        });
};

// Update a activity by the id in the request
exports.update = async (req, res) => {
    console.log(req.body)
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    // console.log(req.body)
    const {title} = req.params;
    // console.log(req.body);
    const {subject, grade, date, time, link} = req.body;
    // console.log(subject, grade, date, time, link);
    let body = {};
    if (subject != '' && subject) {
        body.subject = subject;
    }
    if (grade != '' && grade) {
        body.grade = grade;
    }
    if (date != '' && date) {
        body.date = date;
    }
    if (time != '' && time) {
        body.time = time;
    }
    if (link != '' && link) {
        body.link = link;
    }
    // console.log(body);
    if (title === req.body.title) {
        // console.log("Da");
        await Activity.findOneAndUpdate({"title" : title}, { $set: body})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update activity with title=${title}. Maybe activity was not found!`
                });
            } else {
                res.send({
                    message: "activity was updaded successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not update activity with title=" + title
            });
        });
       
    } else {
        res.status(404).send({
            message: `Permission denied`
        });
    }
};

// Delete a activity with the specified id in the request
exports.delete = async (req, res) => {
    const {title} = req.params;
    await Activity.findOneAndDelete({"title" : title})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete activity with title=${title}. Maybe activity was not found!`
                });
            } else {
                res.send({
                    message: "activity was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete activity with title=" + title
            });
        });
};
