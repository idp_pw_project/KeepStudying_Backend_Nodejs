const client = require("amqplib");

exports.send = async (req, res) => {
	const {activity, link, email} = req.body;
	console.log(req.body);
	console.log(`Hello! \n You joined the ${activity} class! \n Here is your access link: ${link}!`);
    const connection = await client.connect('amqp://username:password@rabbitmq:5672');
	const channel= await connection.createChannel();
	await channel.assertQueue('myQueue');
	channel.sendToQueue('myQueue', Buffer.from(`Hello, ${email}! You joined the ${activity} class. Here is your access link: ${link}.`));
};