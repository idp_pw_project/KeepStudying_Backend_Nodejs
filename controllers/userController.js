const User= require('../models/userModel');

exports.create = (req, res) => {
	//  Validate request
    if (!req.body.userId) {
        res.status(400).send({message: "Content can not be empty!"});
        return;
    }
    const query = req.body.userId;
    // console.log("create");
	// console.log(query);
    User.findOne({userId:query}, function(err, user) {
        if(err) console.log(err);
        if (user) {
            console.log("This has already been saved");
            res.status(400).send({message: "This has already been saved"});
            return;
        } else {
            // Create an User
            const user = new User({
				_id: req.body._id,
                userId: req.body.userId,
				isTeacher: req.body.isTeacher,
				name: req.body.name,
				email: req.body.email,
				phone: req.body.phone,
				grade: req.body.grade,
				age: req.body.age,
				subject: req.body.subject,
            });


            // Save User in the database
            user
                .save(user)
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({
                        message:
                            err.message
                            || "Some error occurred while creating the post."
                    });
                });
        }
    });
   
};

exports.findOne = async (req, res) => {
    const {userId} = req.params;
    await User.find({"userId" : userId})
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Not found user with userId " + userId
                });
            else res.send(data);
        })
        .catch(err => {
            res.status(500)
                .send({
                    message: "Error retrieving user with userId=" + userId
                });
        });
};

exports.findAll = async (req, res) => {
	await User.find()
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message
					|| "Some error occurred while retrieving users."
			});
		});
};

exports.delete = async (req, res) => {
    const {userId} = req.params;
    await User.findOneAndDelete({"userId" : userId})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete user with userId=${userId}. Maybe user was not found!`
                });
            } else {
                res.send({
                    message: "user was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete user with userId=" + userId
            });
        });
};

exports.update = async (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    // console.log(req.body)
    const {userId} = req.params;
    // console.log(req.body);
    const {Name, Email, Phone, Subject, Grade} = req.body;
    // console.log(Name, Email, Phone, Subject, Grade);
    let body = {};
    if (Name != '' && Name) {
        body.name = Name;
    }
    if (Email != '' && Email) {
        body.email = Email;
    }
    if (Phone != '' && Phone) {
        body.phone = Phone;
    }
    if (Subject != '' && Subject) {
        body.subject = Subject;
    }
    if (Grade != '' && Grade) {
        body.grade = Grade;
    }
    console.log(body)
    if (userId === req.body.userId) {
        console.log("Da");
        await User.findOneAndUpdate({"userId" : userId}, { $set: body})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update user with userId=${userId}. Maybe user was not found!`
                });
            } else {
                res.send({
                    message: "user was updaded successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not update user with userId=" + userId
            });
        });
       
    } else {
        res.status(404).send({
            message: `Permission denied`
        });
    }
};