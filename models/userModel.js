const mongoose = require('mongoose');  

const schema = mongoose.Schema(  
    {
        userId: {type: String, required: true, unique: true},
		isTeacher: {type: Boolean, required: true},
        name: {type: String, required: true},
        email: {type: String, required: true},
        phone: {type: String, required: true},
        grade: {type: Number},
        age: {type: Number},
        subject: {type: String}
    }, {
        versionKey: false,
    }
);

schema.method("toJSON", function () {  
    const {__v, _id, ...object} = this.toObject();  
    object.id = _id;  
    return object;  
});  

module.exports = mongoose.model("User", schema);
