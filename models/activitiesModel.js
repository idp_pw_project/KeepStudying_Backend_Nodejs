const mongoose = require('mongoose');  

const schema = mongoose.Schema(  
    {  
        title: {type: String, required: true, unique: true},  
        userId: {type: String, required: true},
        subject: {type: String, required: true},
        grade: {type: Number, required: true},
        date: {type: String, required: true},
        time: {type: String, required: true},
        link: {type: String, required: true},
        maxNoStud: {type: Number, required: true},
        teacher: {type: String}
    }, {
        versionKey: false,
    }
);

schema.method("toJSON", function () {  
    const {__v, _id, ...object} = this.toObject();  
    object.id = _id;  
    return object;  
});  

module.exports = mongoose.model("Activity", schema);
