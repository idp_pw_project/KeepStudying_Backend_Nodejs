const router = require("express").Router();  
const user = require("../controllers/userController.js");  

module.exports = app => {  
    router.get("/", user.findAll);  
    router.get("/:userId", user.findOne);
    router.put("/:userId", user.update);
    router.post("/", user.create);
    router.delete("/:userId", user.delete);
    app.use('/api/users', router);  
};