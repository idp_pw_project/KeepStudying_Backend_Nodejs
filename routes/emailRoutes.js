const router = require("express").Router();  
const email = require("../controllers/emailController.js");  

module.exports = app => {  
    router.post("/", email.send);
    app.use('/api/email', router);  
};