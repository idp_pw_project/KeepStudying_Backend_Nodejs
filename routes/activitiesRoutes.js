const router = require("express").Router();   
const activities = require("../controllers/activityController.js");  

module.exports = app => {  
    router.get("/", activities.findAll);
    router.get("/:title", activities.findOne);
    router.put("/:title", activities.update);
    router.get("/teacher/:userId", activities.findAllByTeacher);
    router.get("/student/:grade", activities.findAllByGrade);
    router.post("/", activities.create);
    router.delete("/:title", activities.delete);
    app.use('/api/activities', router);  
};